package com.pajato.tks.season.adapter

import com.pajato.tks.common.adapter.Strategy
import com.pajato.tks.common.adapter.TmdbFetcher
import com.pajato.tks.common.core.SeasonKey
import com.pajato.tks.season.core.Season
import com.pajato.tks.season.core.SeasonRepo

/**
 * TmdbSeasonRepo is an implementation of the SeasonRepo interface
 * that interacts with the TMDb (The Movie Database) API to fetch
 * season data for TV shows.
 *
 * It uses the TmdbFetcher object to execute network requests and
 * handle interactions with the API.
 */
public object TmdbSeasonRepo : SeasonRepo {

    /**
     * Fetches the specified season of a TV show from TMDb using the given `SeasonKey`.
     *
     * @param key A `SeasonKey` object containing the ID of the TV show and the season number to fetch.
     * @return A `Season` object representing the fetched season data.
     */
    override suspend fun getSeason(key: SeasonKey): Season {
        val path = "tv/${key.id}/season/${key.season}"
        val queryParams: List<String> = listOf()
        val serializer: Strategy<Season> = Season.serializer()
        return TmdbFetcher.fetch(path, queryParams, key, serializer, Season())
    }
}
