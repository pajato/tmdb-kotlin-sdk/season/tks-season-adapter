package com.pajato.tks.season.adapter

import com.pajato.test.ReportingTestProfiler
import com.pajato.tks.common.adapter.TmdbApiService.TMDB_BASE_API3_URL
import com.pajato.tks.common.adapter.TmdbFetcher
import com.pajato.tks.common.core.InjectError
import com.pajato.tks.common.core.SeasonKey
import com.pajato.tks.common.core.jsonFormat
import com.pajato.tks.season.core.Season
import kotlinx.coroutines.runBlocking
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue
import kotlin.test.fail

class SeasonRepoUnitTest : ReportingTestProfiler() {
    private val apiKey = "8G9B0WX31T2U7P0Q"
    private val key: SeasonKey = SeasonKey(424, 1)
    private val url = "$TMDB_BASE_API3_URL/tv/424/season/1?api_key=$apiKey"
    private val resourceName = "torchwood_season_1.json"
    private val urlConverterMap: Map<String, String> = mapOf(url to resourceName)

    private lateinit var errorMessage: String
    private lateinit var errorExc: String
    private lateinit var errorExcMessage: String
    private var isFetched: Boolean = false

    @BeforeTest fun setUp() {
        TmdbFetcher.inject(::fetch, ::handleError)
        TmdbFetcher.inject(apiKey)
        errorMessage = ""
        errorExc = ""
        errorExcMessage = ""
        isFetched = false
    }

    @Test fun `When accessing a non-cached season, verify season is fetched`() {
        val expectedSeason: Season = jsonFormat.decodeFromString(getJson(resourceName))
        runBlocking {
            assertEquals(expectedSeason.id, TmdbSeasonRepo.getSeason(key).id)
            assertTrue(isFetched)
        }
    }

    @Test fun `When simulating a season fetch without injection, verify an inject error is thrown`() {
        val expected = "No api key has been injected!: No repo implementation has been injected!"
        TmdbFetcher.reset()
        runBlocking {
            assertFailsWith<InjectError> { TmdbSeasonRepo.getSeason(key) }.also { assertEquals(expected, it.message) }
        }
    }

    private fun fetch(key: String): String {
        val name = urlConverterMap[key] ?: fail("Key $key is not mapped to a test resource name!")
        isFetched = true
        return getJson(name).ifEmpty { throw IllegalStateException("No JSON available!") }
    }

    private fun getJson(name: String): String = javaClass.classLoader.getResource(name)?.readText() ?: ""

    private fun handleError(message: String, exc: Exception?) {
        errorMessage = message
        errorExc = if (exc == null) "null" else exc.javaClass.name
        errorExcMessage = if (exc == null) "null" else exc.message ?: "null"
    }
}
